# Single Word Classification with Convolutional Neural Networks from Speech Recognition

## Dekripsi
Pada penelitian ini akan ditunjukkan penerapan MFCC sebagai metode untuk ekstraksi fitur suara, serta penerapan algoritma CNN pada pengenalan kata tunggal yang diucapkan dan melakukan perbandingan terhadap dua arsitektur CNN yakni LeNet dan SPNet.

## File pada projek
1. File cnn_metode1.py berfungsi sebagai hyper parameter pada cnn dengan sr:22050, time frame:25ms, Lap:75%
2. File cnn_metode2.py berfungsi sebagai hyper parameter pada cnn dengan sr:16000, time frame:40ms, Lap:50%
3. File main.py berfungsi sebagai fungsi utama yang akan dijalankan pada sistem
4. File __init__.py file diperlukan untuk membuat Python memperlakukan direktori sebagai paket yang berisi.

## Bagaimana cara memulai
Projek ini menggunakan python 2.7 sebagai sistem build dan ubuntu sebagai sistem operasi

Langkah pertama yang harus didownload ialah python versi 2.7
Anda dapat mendownload pada link berikut : https://www.python.org/download/releases/2.7/

Projek ini dapat dibuka dalam beberapa tools yakni : Sublime, Anaconda, Spider, dll. Peneliti menggunakan sublime dalam menjalankan projek ini.

Setelah anda Selesai mendownload python versi 2.7 maka langkah selanjutnya ialah mendownload data set dalam format wave yang telah disediakan oleh Google AI Blog. Url : https://ai.googleblog.com/2017/08/launching-speech-commands%20dataset.html

Setelah itu anda perlu menginstall modul-modul yang diperlukan untuk menjalan sistem. yakni :
1. keras
2. librosa
3. os
4. sklearn
5. matplotlib
6. numpy
7. pandas
8. preprocess
Modul diatas dapat didownload dengan sintaks 'pip install <nama_module>'


## Run Program
Untuk run program dapat menggunakan 'python main.py' pada terminal command prompt.